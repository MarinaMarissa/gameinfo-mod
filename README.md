# GameInfo mod MC-fabric 1.16
It shows information on the game screen, player position, nether/overworld coordinate conversion, address you are viewing, game time and sends you a chat message with the coordinates of the last death point.
# Compile
- git clone https://gitlab.com/ifcifc94/gameinfo-mod.git
- cd gameinfo-mod
- ./gradlew build
