package com.ifcifc.gameinfo.Commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;

public class CommandConfig {
    public static LiteralArgumentBuilder<ServerCommandSource> commandLiteral;
    public static void initialize() {
        commandLiteral = CommandManager.literal("gameinfo").requires((source) -> true);

        CommandReload.register(commandLiteral);
        CommandResetConfig.register(commandLiteral);
        CommandSettings.register(commandLiteral);
        CommandSave.register(commandLiteral);

        CommandRegistry.INSTANCE.register(false, (d)-> d.register(commandLiteral));
    }
}
