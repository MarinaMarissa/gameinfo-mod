package com.ifcifc.gameinfo.Commands;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Config.Menu.Entry;
import com.ifcifc.gameinfo.Config.Options;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import static com.mojang.brigadier.arguments.BoolArgumentType.bool;
import static com.mojang.brigadier.arguments.BoolArgumentType.getBool;
import static com.mojang.brigadier.arguments.IntegerArgumentType.getInteger;
import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class CommandSettings {
    public static void register(LiteralArgumentBuilder<ServerCommandSource> literal) {
        final LiteralArgumentBuilder<ServerCommandSource> conf = CommandManager.literal("settings");

        for(Field F : Options.class.getFields()) {
            if (Modifier.isFinal(F.getModifiers())) continue;
            Entry E = (Entry) Arrays.stream(F.getAnnotations()).filter(An -> An instanceof Entry).findFirst().orElse(null);
            if (E == null) continue;
            switch (E.Type()) {
                case "BooleanToggle":
                    settingBoolean(F, E, conf);
                break;
                case "IntSlider":
                    settingInt(F, E, conf);
                break;
            }
        }
        literal.then(conf);
    }


    public static void settingInt(Field F, Entry E, LiteralArgumentBuilder<ServerCommandSource> conf){
        int max,min;
        try {
            max = (E.CommandMax()!=-1)? E.CommandMax(): (E.Use_max())? Options.class.getField(E.From_max()).getInt(Config.options):E.Max();
            min = (E.CommandMin()!=-1)? E.CommandMin(): (E.Use_min())? Options.class.getField(E.From_min()).getInt(Config.options):E.Min();
        }catch (Exception e){
            max = E.Max();
            min = E.Min();
        }


        conf.then(literal(E.CommandName()).executes((c)->{
            try {
                c.getSource().sendFeedback(new LiteralText(F.getInt(Config.options)+""),false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return 0;
        }).then(argument("value",integer(min,max)) .executes(c->{
            try {
                F.setInt(Config.options,getInteger(c, "value"));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            RenderHUD.toggleHUD();
            return 0;
        })));

    }


    public static void settingBoolean(Field F, Entry E, LiteralArgumentBuilder<ServerCommandSource> conf){
        conf.then(literal(F.getName()).executes((c)->{
            try {
                c.getSource().sendFeedback(new LiteralText(F.getBoolean(Config.options)+""),false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return 0;
        }).then(argument("value",bool()).executes(c->{
            try {
                F.setBoolean(Config.options,getBool(c, "value"));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            RenderHUD.toggleHUD();
            return 0;
        })));
    }
}
