package com.ifcifc.gameinfo.Commands;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;

public class CommandReload {
    public static void register(LiteralArgumentBuilder<ServerCommandSource> literal) {
        literal.then(CommandManager.literal("reload") .executes(CommandReload::execute));
    }

    private static int execute(CommandContext<ServerCommandSource> context) {

        Config.initialize();
        UpdateHUD.reload();
        RenderHUD.toggleHUD();
        context.getSource().sendFeedback(new LiteralText("Reload config"), false);

        return 0;
    }
}
