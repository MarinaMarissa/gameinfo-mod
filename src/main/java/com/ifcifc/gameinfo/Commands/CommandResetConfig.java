package com.ifcifc.gameinfo.Commands;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;

public class CommandResetConfig {
    public static void register(LiteralArgumentBuilder<ServerCommandSource> literal) {
        literal.then(CommandManager.literal("reset") .executes(CommandResetConfig::execute));
    }

    private static int execute(CommandContext<ServerCommandSource> context) {
        Config.createConfig();
        RenderHUD.toggleHUD();
        context.getSource().sendFeedback(new LiteralText("Reset config"), false);

        return 0;
    }
}
