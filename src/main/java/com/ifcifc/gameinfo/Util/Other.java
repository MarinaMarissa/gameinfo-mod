package com.ifcifc.gameinfo.Util;

public class Other {
    public static boolean getBit(int data, int position) {
        return (byte) ((data >> position) & 1) == 1;
    }

    public static double Round(double value, int decimals){
        long d = (long)Math.pow(10, decimals);

        return (double)Math.round(value * d) / d;
    }
}
