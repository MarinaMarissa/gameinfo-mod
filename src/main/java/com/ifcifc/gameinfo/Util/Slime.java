package com.ifcifc.gameinfo.Util;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;

import java.util.Random;

public class Slime {
    public static boolean canSlimeSpawnInChunk(Vec3d pos, long worldSeed) {
        ChunkPos chunkpos = new ChunkPos(new BlockPos(pos.x, 0, pos.z));
        long chunkX=chunkpos.x, chunkZ=chunkpos.z;
        return new Random(worldSeed +
                (chunkX * chunkX *  4987142) + (chunkX * 5947611) +
                (chunkZ * chunkZ) * 4392871L + (chunkZ * 389711) ^ 987234911L).nextInt(10) == 0;
    }
}
