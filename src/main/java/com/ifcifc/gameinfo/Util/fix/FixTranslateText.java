package com.ifcifc.gameinfo.Util.fix;

import net.minecraft.util.Language;

public class FixTranslateText {
    private String KEY, TRANSLATE;
    private Language LAN;

    public FixTranslateText(String key) {
        this.KEY = key;
        this.LAN = Language.getInstance();
        this.update();

    }

    public void update(){
        Language language = Language.getInstance();
        if (language == this.LAN)return;
        this.LAN = language;
        this.TRANSLATE = this.LAN.get(this.KEY);
    }

    public String asString(){
        this.update();
        return this.TRANSLATE;
    }
}
