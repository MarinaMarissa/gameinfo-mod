package com.ifcifc.gameinfo.Util;

import com.ifcifc.gameinfo.Util.fix.FixTranslateText;

public class TranslateText {
    //public static FixTranslateText  DAY;
    //public static FixTranslateText  TIME;
    public static FixTranslateText  DISTANCE;
    public static FixTranslateText  NORTH;
    public static FixTranslateText  SOUTH;
    public static FixTranslateText  EAST;
    public static FixTranslateText  WEST;
    public static FixTranslateText  LAST_DEATH_POSITION;
    public static FixTranslateText  ENABLESHOWINFO;
    public static FixTranslateText  DISABLESHOWINFO;
    public static FixTranslateText  ENABLEDEATHMESSAGE;
    public static FixTranslateText  DISABLEDEATHMESSAGE;
    public static FixTranslateText  HUDMODE;


    public static void initialize(){
        //DAY                     = new FixTranslateText("text.gameinfo.day");
        //TIME                    = new FixTranslateText("text.gameinfo.time");
        DISTANCE                = new FixTranslateText("text.gameinfo.distance");
        NORTH                   = new FixTranslateText("text.gameinfo.north");
        SOUTH                   = new FixTranslateText("text.gameinfo.south");
        EAST                    = new FixTranslateText("text.gameinfo.east");
        WEST                    = new FixTranslateText("text.gameinfo.west");
        LAST_DEATH_POSITION     = new FixTranslateText("text.gameinfo.last_death_position");
        ENABLESHOWINFO          = new FixTranslateText("text.gameinfo.enableshowinfo");
        DISABLESHOWINFO         = new FixTranslateText("text.gameinfo.disableshowinfo");
        ENABLEDEATHMESSAGE      = new FixTranslateText("text.gameinfo.enabledeathmessage");
        DISABLEDEATHMESSAGE     = new FixTranslateText("text.gameinfo.disabledeathmessage");
        HUDMODE                 = new FixTranslateText("text.gameinfo.hudmode");
    }
}
