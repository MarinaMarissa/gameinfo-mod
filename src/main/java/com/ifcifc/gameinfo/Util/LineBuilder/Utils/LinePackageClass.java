package com.ifcifc.gameinfo.Util.LineBuilder.Utils;

public class LinePackageClass {
    public final String name, author, description;
    public final LineClass[] Lines;

    public LinePackageClass(String name, String author, String description, LineClass[] lines) {
        this.name = name;
        this.author = author;
        this.description = description;
        Lines = lines;
    }
}
