package com.ifcifc.gameinfo.Util.LineBuilder.Utils;

import java.util.HashMap;

public class LogicData {
    private String Package;
    private HashMap<String,FunctionData> Functions;

    public LogicData(String aPackage, HashMap<String, FunctionData> functions) {
        Package = aPackage;
        Functions = functions;
    }



    public String getPackage() {
        return Package;
    }

    public FunctionData getFunction(String Function) {
        return Functions.get(Function);
    }

    public String[] getFunctionName(String Function){
        FunctionData functionData = getFunction(Function);
        return new String[]{functionData.Function, functionData.HiddenFuncion};
    }

    public String[] getFunctionPackage(String Function){
        String[] strings = getFunctionName(Function);
        return new String[]{
                this.Package +"." +strings[0],
                this.Package +"." +strings[1]
        };
    }

    public boolean containFunction(String Function){
        return Functions.containsKey(Function);
    }

    public HashMap<String, FunctionData> getFunctions() {
        return Functions;
    }
}
