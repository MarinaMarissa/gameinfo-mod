package com.ifcifc.gameinfo.Util.LineBuilder;

import com.ifcifc.gameinfo.Logic.ModuleController.ModulesController;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.DimensionFormat;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.FunctionData;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LineClass;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LineFormat;
import net.minecraft.util.Language;

public class LineConstructor {
    public LineConstructor() {
    }

    public String buildLine(LineClass format){

        /*String Code =   "var ModulesRegister =  Java.type('main.Logic.ModulesRegister');\n" +
                        "var ret={};\n\n";*/

        String Code="function "+format.Name+"(){\n" +
                "   var lineData='';\n" +
                "   var addSep=false;\n"+
                "   switch(ModulesRegister.getDimensionName()){\n";

        for(DimensionFormat DF : format.Line){

            if(DF.DimensionID.equals("default")){
                Code +="     default:\n";
            }else{
                Code +="     case '"+DF.DimensionID+"':\n";
            }

            Code+=buildFormat(DF.Format, format);
            Code+= "     break;\n";
        }

        Code += "   }\n" +
                "   ret['" + format.Name + "'] = lineData;\n" +
                "}\n";

        return Code;
    }

    private String buildFormat(LineFormat[] Fr, LineClass format){
        String Code="";
        String lastHidden="";
        boolean addSep = false;

        final Language instance = Language.getInstance();

        for(LineFormat F : Fr){
            F.check();

            String text = F.Text;
            String endText = F.EndText;
            String separated = F.Separated;
            String notSeparated = F.NotSeparated;
            if(instance!=null && !F.TranslateText.trim().isEmpty()){
                text            = text.replace("$Translated$",instance.get(F.TranslateText));
                endText         = endText.replace("$Translated$",instance.get(F.TranslateText));
                separated       = separated.replace("$Translated$",instance.get(F.TranslateText));
                notSeparated    = notSeparated.replace("$Translated$",instance.get(F.TranslateText));
            }

            if(F.isText){
                Code += "       lineData +='" + text + "';\n";
            }else{
                final FunctionData Function = ModulesController.getFunction(F.Function);

                if(Function==null){
                    System.out.println("Bad function in line(" +format.Name+ ")" +
                            ((F.DebugName.isEmpty())? "":"{" + F.DebugName + "}") +
                            "<" + F.Function + ">");
                    continue;
                }

                String Argument = F.Argument;

                if(!Argument.isEmpty()){

                    if(!Function.acceptArguments || !checkArguments(Argument)){

                        System.out.println("Bad Argument in line(" +format.Name+ ")" +
                                ((F.DebugName.isEmpty())? "":"{" + F.DebugName + "}") +
                                "<" + Argument + ">");
                        if(!Function.acceptArguments){
                            System.out.println("Function(" + Function.Function + ") not accept Arguments");
                        }

                        continue;
                    }
                }else if(Function.acceptArguments){
                    Argument = Function.defaultArguments;
                }

                if(!lastHidden.isEmpty() && lastHidden.equals(Function.getHiddenFuncion())){
                    Code = Code.substring(0, Code.length()-2) + "\n";
                }else{
                    Code += "       if(!ModulesRegister."+Function.getHiddenFuncion()+"()){\n";
                    addSep = true;
                }

                lastHidden = Function.getHiddenFuncion();
                Code += "           lineData += ";
                if(!separated.isEmpty() || !notSeparated.isEmpty()){
                    Code += "((addSep)? '"+ separated +"' : '"+ notSeparated +"') + ";
                }

                if(!text.isEmpty()){
                    Code += "'" + text + "' + ";
                }

                Code += "ModulesRegister."+Function.getFunction() + ((Argument.isEmpty())? "()" : ("(" + Argument +")"));

                if(!endText.isEmpty()){
                    Code += " + '" + endText + "'";
                }

                Code += ";\n";

                if(addSep){
                   addSep=false;
                   Code += "           addSep=true;\n";
                }

                Code += "       }";
                if(F.isDependSep){
                    Code += "else{\n"+
                            "           addSep=false;\n"+
                            "       }\n";
                }else{
                    Code+="\n";
                }


            }
        }

        return Code;
    }

    private boolean checkArguments(String arg){
        String[] Args = arg.split(",");

        if(Args.length>64)return false;

        for(String A : Args){
            if(A.length()>32 || A.isEmpty()){
                return false;
            }
        }

        return true;
    }

}