package com.ifcifc.gameinfo.Config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ifcifc.gameinfo.Util.WorldUtil;
import net.fabricmc.loader.api.FabricLoader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

public class Config {
    public static   Options options = null;

    public static Gson gson;

    public static   String  DeathPath, GamePath, HUDPath;
    private static  File    fileJSON;

    public static void initialize() {
        fileJSON = new File(FabricLoader.getInstance().getConfigDir().toString(),"gameinfo.json");
        DeathPath = FabricLoader.getInstance().getGameDir().toString();//+"/deathpoints/";
        DeathPath =(DeathPath.charAt(DeathPath.length()-1)=='.')? DeathPath.substring(0, DeathPath.length()-1): DeathPath;

        if(DeathPath.substring((DeathPath.length()-1))!="/"){
            DeathPath+= "/";
        }

        GamePath = DeathPath;
        HUDPath = GamePath + "gameinfo-resource/";
        DeathPath +=((DeathPath.charAt(DeathPath.length()-1)=='/')? "":"/")+"deathpoints/";
        gson = new GsonBuilder().setPrettyPrinting().create();

        System.out.println("HUD Path: " + HUDPath);

        existPath();
        existJSON();
    }


    public static boolean existPath(){
        final File F = new File(DeathPath);
        if(!F.exists()) return F.mkdir();
        return F.exists();
    }

    public static boolean existJSON() {
        try {
            if (fileJSON.exists()) {
                HashMap<String,Object> map = gson.fromJson(new FileReader(fileJSON), HashMap.class);

                if(loadSettings(map)){
                    System.out.println("GameInfo: Load Config");
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("GameInfo: Error with config file");
        }
        System.out.println("GameInfo: Create Config");
        return createConfig();
    }

    public static boolean createConfig(){
        options = new Options();
        return Config.updateSave();
    }

    public static boolean loadSettings(HashMap<String, Object> map){
        Options O = new Options();
        try {
            for(Field f : Options.class.getFields()){
                if(Modifier.isFinal(f.getModifiers()))continue;
                String name = f.getName();
                if(map.containsKey(name)){

                    switch (f.getType().getTypeName()){
                        case "int":
                            f.set(O, (int)((double)map.get(name)));
                            break;
                        case "long":
                            f.set(O, (long)((double)map.get(name)));
                            break;
                        case "double":
                            f.set(O, map.get(name));
                            break;
                        case "float":
                            f.set(O, (float)((double)map.get(name)));
                            break;
                        case "boolean":
                            f.set(O, map.get(name));
                            break;
                        case "java.lang.String":
                            f.set(O, map.get(name));
                            break;

                        default:
                            System.out.println("Type not defined in config: " + f.getType().getTypeName());
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }

        Config.options = O;

        options.Update();
        return true;
    }


    public static boolean updateSave() {
        options.Update();
        HashMap<String, Object> map = new HashMap<>();
        try {
            for(Field f : Options.class.getFields()){
                map.put(f.getName(), f.get(Config.options));
            }

            final FileWriter sFile = new FileWriter(fileJSON);
            sFile.write(gson.toJson(map));
            sFile.close();
        } catch (Exception e) {
            System.out.println("GameInfo: ERROR SAVE CONFIG");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static void updateDeathLog(String LOG){
        String levelName = WorldUtil.getLevelName()+".txt";

        if(!existPath()){
            System.out.println("Error folder <" + DeathPath + "> can't create.");
            return;
        }

        try {
            final FileWriter sFile = new FileWriter(new File(DeathPath,levelName), true);

            sFile.write(LOG);
            sFile.write("\n");

            sFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
