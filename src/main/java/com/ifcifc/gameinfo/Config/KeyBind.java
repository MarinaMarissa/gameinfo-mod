package com.ifcifc.gameinfo.Config;

import com.ifcifc.gameinfo.Config.Menu.SettingMenu;
import com.ifcifc.gameinfo.MainMod;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.ifcifc.gameinfo.Util.PackageDebug;
import com.ifcifc.gameinfo.Util.TranslateText;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;

public class KeyBind {
    public static KeyBinding KEYToggleShow = new KeyBinding("key.gameinfo.toggleshow", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_I, "GameInfo");
    public static KeyBinding KEYShowDeathMessage = new KeyBinding("key.gameinfo.deathmessage", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_UNKNOWN, "GameInfo");
    public static KeyBinding KEYOpenSettings = new KeyBinding("key.gameinfo.opensettings", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_P, "GameInfo");
    public static KeyBinding KEYRotateHud = new KeyBinding("key.gameinfo.rotatehud", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_UNKNOWN, "GameInfo");

    public static KeyBinding KEYChangeDebugPackageUP = new KeyBinding("key.gameinfo.debugup", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_UNKNOWN, "GameInfo");
    public static KeyBinding KEYChangeDebugPackageDown = new KeyBinding("key.gameinfo.debugdown", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_UNKNOWN, "GameInfo");


    public static void initialize() {
        KeyBindingHelper.registerKeyBinding(KEYToggleShow);
        KeyBindingHelper.registerKeyBinding(KEYShowDeathMessage);
        KeyBindingHelper.registerKeyBinding(KEYOpenSettings);
        KeyBindingHelper.registerKeyBinding(KEYRotateHud);
        KeyBindingHelper.registerKeyBinding(KEYChangeDebugPackageUP);
        KeyBindingHelper.registerKeyBinding(KEYChangeDebugPackageDown);


        ClientTickEvents.START_CLIENT_TICK.register(tick->{

            if(null==tick.player)return;

            if(KEYChangeDebugPackageUP.wasPressed()) PackageDebug.changePackage(true);
            if(KEYChangeDebugPackageDown.wasPressed()) PackageDebug.changePackage(false);

            if(KEYOpenSettings.wasPressed()){
                tick.openScreen(SettingMenu.getConfigBuilderScreen().build());
            }

            if (KEYShowDeathMessage.wasPressed()) {
                Config.options.ShowDeathMessage = !Config.options.ShowDeathMessage;
                Config.updateSave();
                MainMod.sendMessage("§l§e" + ((Config.options.ShowDeathMessage?
                                TranslateText.ENABLEDEATHMESSAGE.asString():
                                TranslateText.DISABLEDEATHMESSAGE.asString())),
                        true);
            }

            if (KEYToggleShow.wasPressed()) {
                //                tick.getToastManager().add(new SystemToast(null, new LiteralText("Wiii"), new LiteralText("Waaa")));
                RenderHUD.toggleHiddenHud(true,false);
                Config.updateSave();
                MainMod.sendMessage("§l§e" + ((Config.options.Hidden?
                                TranslateText.DISABLESHOWINFO.asString():
                                TranslateText.ENABLESHOWINFO.asString())),
                        true);
            }

            if(KEYRotateHud.wasPressed()){
                RenderHUD.rotateHUD();
            }


            /*if(Add.wasPressed())RenderHUD.Add++;
            if(Sub.wasPressed())RenderHUD.Add--;
            if(Sve.wasPressed()){
                File file = new File("/home/igna/Escritorio/Points.txt");
                FileWriter fr = null;
                String D = "(" +(0.5f+(Config.options.ScaleX-1)*0.005f)+", "+ RenderHUD.Add+")\n";
                try {
                    fr = new FileWriter(file, true);
                    fr.write(D);
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                RenderHUD.Add=0;
            }
            if(Inc.wasPressed())Config.options.ScaleX++;
            if(Dec.wasPressed())Config.options.ScaleX--;*/
        });
    }
}
