package com.ifcifc.gameinfo.Config;

import com.ifcifc.gameinfo.Config.Menu.Entry;

public class Options {
    public final int    version                     = 11;
    @Entry(From_max = "HUDLIMIT", Use_max = true, CommandMax=15, Type = "IntSlider",CommandName = "HudMode")
    public int          HUDINFOSHOW                 = 15;
    public int          HUDLIMIT                    = 15;

    @Entry(CommandName = "DisableAutoJump")
    public boolean      DisableAutoJump             = true;

    @Entry(CommandName = "ShowDeathMessage")
    public boolean      ShowDeathMessage            = true;

    @Entry(CommandName = "Hidden")
    public boolean      Hidden                      = false;

    @Entry(CommandName = "HiddenSpeed")
    public boolean      HiddenSpeed                 = false;

    @Entry(CommandName = "HiddenBiome")
    public boolean      HiddenBiome                   = false;

    @Entry(CommandName = "HiddenFPS")
    public boolean      HiddenFPS                   = false;

    @Entry(CommandName = "HiddenTicks")
    public boolean      HiddenTicks                 = false;

    @Entry(CommandName = "HiddenLight")
    public boolean      HiddenLight                 = false;
    @Entry(CommandName = "ShowBlockLight")
    public boolean      ShowBlockLight              = true;
    @Entry(CommandName = "ShowSunLight")
    public boolean      ShowSunLight                = true;
    @Entry(CommandName = "HiddenSunLightIfZero")
    public boolean      HiddenSunLightIfZero        = true;
    /////////////////////////////////////////////////////////////////////////
    @Entry(Category = "UI", CommandName = "RightHUD")
    public boolean      RightHUD                = false;
    @Entry(Category = "UI", CommandName = "DownHUD")
    public boolean      DownHUD = false;
    @Entry(Category = "UI", CommandName = "sortByWidth")
    public boolean      sortByWidth             = false;
    @Entry(Category = "UI", CommandName = "sortInvest")
    public boolean      sortInvest              = false;
    @Entry(Category = "UI", CommandName = "EditHud")
    public boolean      EditHud                = false;

    @Entry(Category = "UI", CommandName = "useScale")
    public boolean      useScale            = false;
    @Entry(Category = "UI", Max = 300, Type = "IntSlider", CommandName = "ScaleX")
    public int          ScaleX              = 100;
    @Entry(Category = "UI", Max = 300, Type = "IntSlider", CommandName = "ScaleY")
    public int          ScaleY              = 100;
    ////////////////////////////////////////////////////////////////////////
    @Entry(Category = "lightalert", CommandName = "AlertLowLightLevel")
    public boolean      AlertLowLightLevel          = false;

    @Entry(Category = "lightalert", CommandName = "PlaySoundAlertLowLevel")
    public boolean      PlaySoundAlertLowLevel      = false;

    @Entry(Category = "lightalert", CommandName = "LowLevelSunAndBlock")
    public boolean      LowLevelSunAndBlock         = false;

    @Entry(Category = "lightalert",Min=0,Max = 15, Type = "IntSlider", CommandName = "AlertMinLightLevel")
    public int          AlertMinLightLevel          = 7;

    @Entry(Category = "lightalert",Min=0,Max = 15, Type = "IntSlider", CommandName = "SoundAlertMinLightLevel")
    public int          SoundAlertMinLightLevel     = 2;

    @Entry(Category = "lightalert",Max = 300, Type = "IntSlider", CommandName = "AlertTickDelay")
    public int          AlertTickDelay              = 20;

    @Entry(Category = "lightalert", CommandName = "ToastAlertLowLight")
    public boolean      ToastAlertLowLight          = true;

    public String HUDFILE = "defaultHUD";

    /////////////////////////////////////////////////////////////////////////
    @Entry(Category = "Modules", CommandName = "DebugPackages")
    public boolean      DebugPackages                = false;

    /////////////////////////////////////////////////////////////////////////

    public Options(){
    }
    //////////////////////////////////////////////////////////////////
    public transient float _ScaleX=0, _ScaleY=0;

    public void Update(){
        _ScaleX=(0.5f+(Config.options.ScaleX-1)*0.005f);
        _ScaleY=(0.5f+(Config.options.ScaleY-1)*0.005f);


    }


    public float getScaleX(){
        return _ScaleX;
    }
    public float getScaleY(){
        return _ScaleY;
    }


}
