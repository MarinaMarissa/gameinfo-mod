package com.ifcifc.gameinfo.Config.Menu;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Config.Options;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModulesRegister;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.Render.RenderHUD;
import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.BooleanListEntry;
import me.shedaniel.clothconfig2.impl.builders.SubCategoryBuilder;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;

public class SettingMenu {
    @SuppressWarnings("rawtypes")
    public static ConfigBuilder getConfigBuilderScreen(){
        final Options default_options = new Options();
        ConfigBuilder builder = ConfigBuilder.create().transparentBackground().setTitle(new TranslatableText("config.gameinfo.title"));

        HashMap<String, SubCategoryBuilder> subCategory = new HashMap<>();

        ConfigEntryBuilder eb = builder.entryBuilder();
        ConfigCategory general = builder.getOrCreateCategory(new TranslatableText("config.gameinfo.title"));
        //System.out.println("--------------------------------------------------------------");

        try {
            for(Field F : Options.class.getFields()){
                if(Modifier.isFinal(F.getModifiers()))continue;
                Entry E = (Entry) Arrays.stream(F.getAnnotations()).filter(An -> An instanceof Entry).findFirst().orElse(null);
                if(E==null)continue;

                AbstractConfigListEntry t_Entry = null;

                String Key = "config.gameinfo."+F.getName().toLowerCase();
                //System.out.println(Key);
                switch (E.Type()){
                    case "BooleanToggle":
                    t_Entry=eb.startBooleanToggle(new TranslatableText(Key), F.getBoolean(Config.options))
                            .setDefaultValue(F.getBoolean(default_options))
                            .setSaveConsumer(v-> {
                                try {
                                    F.set(Config.options, v);
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            })
                            .build();
                    break;
                    case "IntSlider":
                        int max = (E.Use_max())? Options.class.getField(E.From_max()).getInt(Config.options):E.Max(),
                            min = (E.Use_min())? Options.class.getField(E.From_min()).getInt(Config.options):E.Min();

                        t_Entry=eb.startIntSlider(new TranslatableText(Key), F.getInt(Config.options), min,  max)
                                .setDefaultValue(F.getInt(default_options))
                                .setTextGetter(integer -> new LiteralText("Mode: " + integer))
                                .setSaveConsumer(v-> {
                                    try {
                                        F.set(Config.options, v);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                })
                                .build();
                    break;
                }

                if(E.Category().equals("general")){
                    general.addEntry(t_Entry);
                }else{
                    if(!subCategory.containsKey(E.Category())){
                        subCategory.put(E.Category(), eb.startSubCategory(new TranslatableText("config.gameinfo."+E.Category())).setExpanded(false));
                    }
                    subCategory.get(E.Category()).add(t_Entry);
                }


                

            }
            modules(eb, subCategory.get("Modules"));
            if(Config.options.EditHud) {
                subCategory.get("UI").add(eb.startTextField(new LiteralText("HUD File:"), Config.options.HUDFILE).setDefaultValue(default_options.HUDFILE).setSaveConsumer(s -> {
                    if (!Config.options.HUDFILE.equals(s)) {
                        Config.options.HUDFILE = s;
                        final File file = new File(Config.HUDPath, Config.options.HUDFILE + ".json");

                        if (!file.exists()) {
                            Config.options.HUDFILE = default_options.HUDFILE;
                        }
                        UpdateHUD.reload();
                    }
                }).build());
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        subCategory.forEach((k,v)-> general.addEntry(v.build()));


        return builder.setSavingRunnable(() -> {
            if(!Config.options.EditHud && !Config.options.HUDFILE.equals(default_options.HUDFILE)){
                Config.options.HUDFILE = default_options.HUDFILE;
            }

            UpdateHUD.reload();

            RenderHUD.hiddenHud(true, true, true);
            RenderHUD.sortLines();
            RenderHUD.toggleHUD();
            RenderHUD.hiddenHud(false, true, false);
            Config.updateSave();
            Config.initialize();
        });
    }

    public static void modules(ConfigEntryBuilder eb, SubCategoryBuilder cat){
        for(ModuleBase M : ModulesRegister.ModulesList) {
            //Entry E = (Entry) Arrays.stream(M.getAnnotations()).filter(An -> An instanceof Entry).findFirst().orElse(null);
            final ModuleRegNameAnnotation A = M.getClass().getAnnotation(ModuleRegNameAnnotation.class);
            BooleanListEntry t_Entry = eb.startBooleanToggle(new LiteralText("Package " + A.RegName()), M.isEnable())
                    .setDefaultValue(true)
                    .setSaveConsumer(v -> {
                        M.setEnable(v);
                    })
                    .build();
            cat.add(t_Entry);
        }
    }
}
