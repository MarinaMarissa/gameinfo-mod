package com.ifcifc.gameinfo.Config.Menu;

import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import net.minecraft.client.gui.screen.Screen;

public class ModMenuEntry  implements ModMenuApi {

    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return this::getScreen;
    }


    public Screen getScreen(Screen parent){
        return SettingMenu.getConfigBuilderScreen().setParentScreen(parent).build();
        //return new testScreen();
    }


}