package com.ifcifc.gameinfo.Config.Menu;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Entry {
    String Category()       default "general";
    String Type()           default "BooleanToggle";
    String CommandName()    default "None";
    int Min()               default 1;
    int Max()               default 1;
    int CommandMax()        default -1;
    int CommandMin()        default -1;
    boolean Use_min()       default false;
    boolean Use_max()       default false;
    String  From_min()      default "";
    String  From_max()      default "";
}
