package com.ifcifc.gameinfo;

import com.ifcifc.gameinfo.Commands.CommandConfig;
import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Config.KeyBind;
import com.ifcifc.gameinfo.Logic.LoadHUD;
import com.ifcifc.gameinfo.Logic.ModuleController.ModulesController;
import com.ifcifc.gameinfo.Logic.ModuleController.ModulesRegister;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.ifcifc.gameinfo.Util.PackageDebug;
import com.ifcifc.gameinfo.Util.TranslateText;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

public class MainMod implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		Config.initialize();
		TranslateText.initialize();
		ModulesController.initialice();
		ModulesRegister.initialice();
		CommandConfig.initialize();
		RenderHUD.initialize();
		KeyBind.initialize();
		UpdateHUD.initialize();
		LoadHUD.initialize();
		PackageDebug.initialize();

		//ModulesController.saveJSON();
		//System.exit(0);

//		ModulesController.savePackages();
	}


	public static void sendMessage(String text, boolean actionBar){
		MainMod.sendMessage(new LiteralText(text), actionBar);
	}

	public static void sendMessage(Text text, boolean actionBar){
		try {
			if (actionBar) {
				MinecraftClient.getInstance().inGameHud.setOverlayMessage(text, false);
			} else {
				MinecraftClient.getInstance().inGameHud.getChatHud().addMessage(text);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



}

