package com.ifcifc.gameinfo.UI;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.Element;
import net.minecraft.client.util.math.MatrixStack;

import java.util.ArrayList;

@Environment(EnvType.CLIENT)
public class SimpleScrollWidget extends DrawableHelper implements Drawable, Element {
    int x, y, width, height, maxRender, index;

    public ArrayList<SimpleDrawable> childs;

    public SimpleScrollWidget(int x, int y, int width, int height, int maxRender, int index) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.index = index;
        this.maxRender = maxRender;
        this.childs = new ArrayList<>();
    }

    public void scroll(int amount){
        index+=amount;
        if(index<0)index=0;
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        if(this.childs.size()==0)return;
        if(this.childs.size()<=maxRender){
            int i=0;
            for (SimpleDrawable D : this.childs){
                i++;
                D.render(matrices, mouseX, mouseY, x,y+i*16, delta);
            }
        }else{
            int e =0;
            for(int i=index;i<(index+maxRender);i++){
                if(i>=childs.size())break;
                this.childs.get(i).render(matrices, mouseX, mouseY, x,y+e*16, delta);
                e++;
            }
        }

    }

    @Override
    public void mouseMoved(double mouseX, double mouseY) {

    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        return false;
    }

    @Override
    public boolean mouseReleased(double mouseX, double mouseY, int button) {
        return false;
    }

    @Override
    public boolean mouseDragged(double mouseX, double mouseY, int button, double deltaX, double deltaY) {
        return false;
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
        System.out.println(amount);
        return false;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        return false;
    }

    @Override
    public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
        return false;
    }

    @Override
    public boolean charTyped(char chr, int keyCode) {
        return false;
    }

    @Override
    public boolean changeFocus(boolean lookForwards) {
        return false;
    }

    @Override
    public boolean isMouseOver(double mouseX, double mouseY) {
        return false;
    }
}
