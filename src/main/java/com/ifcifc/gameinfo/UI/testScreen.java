package com.ifcifc.gameinfo.UI;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;

@Environment(EnvType.CLIENT)
public class testScreen extends Screen {
    SimpleScrollWidget SSW;
    public testScreen() {
        super(new LiteralText("TEST"));
        this.init();
    }

    protected void init() {
        this.initWidgets();
    }

    public void initWidgets(){

        this.addButton(new ButtonWidget(this.width / 2 - 102, this.height / 4 + 24 + -16, 204, 20, new LiteralText("Return"), (buttonWidgetx) -> {
            //this.client.openScreen((Screen)null);
            //this.client.mouse.lockCursor();
        }));

        SSW = this.addChild(new SimpleScrollWidget(this.width / 2 - 102, this.height / 4 + 24 + -32,100,100, 24,0));
        for(int e=0;e<100;e++){
            int finalE = e;
            SSW.childs.add(new SimpleDrawable(){
                @Override
                public void render(MatrixStack matrices, int mouseX, int mouseY, int rX, int rY, float delta) {
                    this.drawCenteredString(matrices, MinecraftClient.getInstance().textRenderer,("weee"+ finalE),rX, rY, 16777215);
                }
            });
        }
    }

    public void tick() {
        super.tick();
    }


    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        this.renderBackground(matrices);
        this.drawCenteredText(matrices, this.textRenderer, this.title, this.width / 2, 8, 16777215);
        super.render(matrices, mouseX, mouseY, delta);

        this.children.forEach(e->{
            if(e instanceof SimpleScrollWidget){
                ((SimpleScrollWidget)e).render(matrices, mouseX, mouseY, delta);
            }
        });

        //System.out.println(this.mouseClicked(this.width / 2 - 102, this.height / 4 + 24 + -16,0));
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
        this.SSW.scroll((int)amount);

        return super.mouseScrolled(mouseX, mouseY, amount);
    }
}
