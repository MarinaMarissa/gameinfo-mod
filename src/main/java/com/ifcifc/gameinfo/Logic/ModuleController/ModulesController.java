package com.ifcifc.gameinfo.Logic.ModuleController;

import com.ifcifc.gameinfo.Util.LineBuilder.Utils.FunctionData;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LogicData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class ModulesController {
    public static ArrayList<LogicData> ModulesList;

    public static void initialice(){
        ModulesList = new ArrayList<>();
    }

    public static void register(ModuleBase M){
        HashMap<String, FunctionData> Functions = new HashMap<>();
        String Package = M.getClass().getAnnotation(ModuleRegNameAnnotation.class).RegName();

        for(Method f : M.getClass().getMethods()){
            ModuleRegisterAnnotation E = f.getAnnotation(ModuleRegisterAnnotation.class);
            if (E == null) continue;
            FunctionData Fd = new FunctionData(f.getName(), E.isHiddenFuncion(), Package, E.defaultArguments(), E.description(), E.Arguments(), E.acceptArguments(), E.isNeededArguments());
            Functions.put(E.Funcion(), Fd);
        }

        ModulesList.add(new LogicData(Package, Functions));

    }


    public static void showPackages(){
        ModulesList.forEach(P->{
            System.out.println("Pakage: "+P.getPackage());
            P.getFunctions().forEach((k,v)->{
                System.out.println("    "+k + ": " + v.Function + " - " + v.HiddenFuncion + " - " + v.acceptArguments + " - " + v.isNeededArguments);
            });
        });
    }

    public static void savePackages(){
        final String[] CSV = {""};
        ModulesList.forEach(P->{
            //System.out.println("Pakage: "+P.getPackage());
            P.getFunctions().forEach((k,v)->{
                CSV[0] += v.Package + "¬ " + k + "¬ " + v.acceptArguments + "¬ " + v.isNeededArguments + "¬ " + v.defaultArguments + "¬ "  + v.Arguments + "¬ " + v.description + "\n";
            });
        });

        try {
            FileWriter sFile = new FileWriter(new File( "Packages.csv"));
            sFile.write(CSV[0]);
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveJSON(){
        final String[] CSV = {"["};
        ModulesList.forEach(P->{
            P.getFunctions().forEach((k,v)->{
                CSV[0] += "{\"function\":\"" + k + "\", \"arg\": " + v.acceptArguments + ", \"description\":\"" + v.description + "\"},\n";
                // CSV[0] += v.Package + "¬ " + k + "¬ " + v.acceptArguments + "¬ " + v.isNeededArguments + "¬ " + v.defaultArguments + "¬ "  + v.Arguments + "¬ " + v.description + "\n";
            });
        });


        CSV[0] += "]";
        try {
            FileWriter sFile = new FileWriter(new File( "Packages.json"));
            sFile.write(CSV[0]);
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static FunctionData getFunction(String Function) {
        LogicData Ld = getModule(Function);
        if(Ld==null)return null;

        return Ld.getFunction(Function);
    }

    public static LogicData getModule(String Function){
        for (LogicData e : ModulesList) {
            if (e.containFunction(Function)) {
                return  e;
            }
        }

        return null;
    }

    public static String[] getFunctionPackage(String Function){
        return getModule(Function).getFunctionPackage(Function);
    }
}
