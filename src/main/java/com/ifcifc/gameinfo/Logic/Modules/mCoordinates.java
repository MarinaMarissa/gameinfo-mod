package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import com.ifcifc.gameinfo.Util.Slime;
import com.ifcifc.gameinfo.Util.TranslateText;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;

import static com.ifcifc.gameinfo.Util.Other.Round;

@ModuleRegNameAnnotation(RegName="Coordinates")
public class mCoordinates implements ModuleBase {

    public Vec3d        vPos, oldPos;
    public BlockPos     Pos;
    public Direction    direction;

    public boolean isEnable = true;

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        oldPos      = vPos;
        if(oldPos==null)oldPos = tick.getCameraEntity().getPos();
        vPos        = tick.getCameraEntity().getPos();
        Pos         = tick.getCameraEntity().getBlockPos();
        direction   = tick.getCameraEntity().getHorizontalFacing();
    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @ModuleRegisterAnnotation(Funcion = "X", description = "Get player X position with decimals")
    public double getX() {
        return Round(vPos.x,2);
    }

    @ModuleRegisterAnnotation(Funcion = "Y", description = "Get player Y position with decimals")
    public double getY() {
        return Round(vPos.y,2);
    }

    @ModuleRegisterAnnotation(Funcion = "Z", description = "Get player Z position with decimals")
    public double getZ() {
        return Round(vPos.z,2);
    }

    @ModuleRegisterAnnotation(Funcion = "RX", description = "Get player round X position")
    public long getRX() {
        return Math.round(getX());
    }

    @ModuleRegisterAnnotation(Funcion = "RY", description = "Get player round Y position")
    public long getRY() {
        return Math.round(getY());
    }

    @ModuleRegisterAnnotation(Funcion = "RZ", description = "Get player round Z position")
    public long getRZ() {
        return Math.round(getZ());
    }

    @ModuleRegisterAnnotation(Funcion = "BX", description = "Get player X block position")
    public long getBX() {
        return Pos.getX();
    }

    @ModuleRegisterAnnotation(Funcion = "BY", description = "Get player Y block position")
    public long getBY() {
        return Pos.getY();
    }

    @ModuleRegisterAnnotation(Funcion = "BZ", description = "Get player Z block position")
    public long getBZ() {
        return Pos.getZ();
    }

//////////////////////////////////////////////
    @ModuleRegisterAnnotation(Funcion = "OX", description = "Get player X position in Overworld from nether")
    public double getOX() {
        return getX()*8;
    }

    @ModuleRegisterAnnotation(Funcion = "OZ", description = "Get player Z position in Overworld from nether")
    public double getOZ() {
        return getZ()*8;
    }

    @ModuleRegisterAnnotation(Funcion = "ORX", description = "Get player round X position in Overworld from nether")
    public long getORX() {
        return Math.round(getOX());
    }


    @ModuleRegisterAnnotation(Funcion = "ORZ", description = "Get player round Z position in Overworld from nether")
    public long getORZ() {
        return Math.round(getOZ());
    }

    @ModuleRegisterAnnotation(Funcion = "OBX", description = "Get player X block position in Overworld from nether")
    public long getOBX() {
        return Pos.getX()*8;
    }

    @ModuleRegisterAnnotation(Funcion = "OBZ", description = "Get player Z block position in Overworld from nether")
    public long getOBZ() {
        return Pos.getZ()*8;
    }
//////////////////////////////////////////////

    @ModuleRegisterAnnotation(Funcion = "NX", description = "Get player X position in nether")
    public double getNX() {
        return getX()/8;
    }

    @ModuleRegisterAnnotation(Funcion = "NZ", description = "Get player Z position in nether")
    public double getNZ() {
        return getZ()/8;
    }

    @ModuleRegisterAnnotation(Funcion = "NRX", description = "Get player round X position in nether")
    public long getNRX() {
        return Math.round(getNX());
    }

    @ModuleRegisterAnnotation(Funcion = "NRZ", description = "Get player round Z position in nether")
    public long getNRZ() {
        return Math.round(getNZ());
    }

    @ModuleRegisterAnnotation(Funcion = "NBX", description = "Get player X block position in nether")
    public long getNBX() {
        return Pos.getX()/8;
    }

    @ModuleRegisterAnnotation(Funcion = "NBZ", description = "Get player Z block position in nether")
    public long getNBZ() {
        return Pos.getZ()/8;
    }
//////////////////////////////////////////////

    @ModuleRegisterAnnotation(Funcion = "Direccion", description = "Direction in which the player sees")
    public String getDireccion(){
        return direction.asString();
    }

    @ModuleRegisterAnnotation(Funcion = "FixDireccion", description = "Direction in which the player sees, in one char")
    public String getFixDireccion(){
        switch(direction) {
            case NORTH:
                return TranslateText.NORTH.asString();
            case SOUTH:
                return TranslateText.SOUTH.asString();
            case WEST:
                return TranslateText.WEST.asString();
            default:
                return TranslateText.EAST.asString();
        }
    }

////////////////////////////////////////////////////////
    @ModuleRegisterAnnotation(Funcion = "Coords", description = "Get all player coords")
    public String getCoords(){
        return getX() + " " + getY() + " " + getZ();
    }
    @ModuleRegisterAnnotation(Funcion = "RCoords", description = "Get all player round coords")
    public String getRCoords(){
        return getRX() + " " + getRY() + " " + getRZ();
    }
    @ModuleRegisterAnnotation(Funcion = "BCoords", description = "Get all block coords")
    public String getBCoords(){
        return getBX() + " " + getBY() + " " + getBZ();
    }

    @ModuleRegisterAnnotation(Funcion = "NCoords", description = "Get all player nether coords from nether")
    public String getNCoords(){
        return getNX()   + " " + getNZ();
    }
    @ModuleRegisterAnnotation(Funcion = "NRCoords", description = "Get all player nether round coords from nether")
    public String getNRCoords(){
        return getNRX() + " " + getNRZ();
    }
    @ModuleRegisterAnnotation(Funcion = "NBCoords", description = "Get all player nether block coords from nether")
    public String getNBCoords(){
        return getNBX() + " " + getNBZ();
    }

    @ModuleRegisterAnnotation(Funcion = "OCoords", description = "Get all player overworld coords from nether")
    public String getOCoords(){
        return getOX()   + " " + getOZ();
    }
    @ModuleRegisterAnnotation(Funcion = "ORCoords", description = "Get all player overworld round coords from nether")
    public String getORCoords(){
        return getORX() + " "  + getORZ();
    }
    @ModuleRegisterAnnotation(Funcion = "OBCoords", description = "Get all player overworld block coords from nether")
    public String getOBCoords(){
        return getOBX() + " " + getOBZ();
    }
////////////////////////////////////////////////////////

    @ModuleRegisterAnnotation(Funcion = "Slime", isHiddenFuncion = "isHiddenSlime", description = "is Slime chunk")
    public String isSlime(){
        return "Slime Chunk";
    }

    @ModuleRegisterAnnotation(Funcion = "Biome", isHiddenFuncion = "isHiddenBiome", description = "Get biome name")
    public String getBiome(){
        final ClientWorld world = MinecraftClient.getInstance().world;
        return world.getRegistryManager().get(Registry.BIOME_KEY).getId(world.getBiome(Pos)).getPath().replace("_", " ");
    }

    @ModuleRegisterAnnotation(Funcion = "Vel", isHiddenFuncion = "isHiddenVel", description = "Get speed per tick")
    public double getVel() {
        final double v = oldPos.multiply(1, 0, 1).distanceTo(vPos.multiply(1, 0, 1));

        return Round(v, 2);
    }

    @ModuleRegisterAnnotation(Funcion = "VelBs", isHiddenFuncion = "isHiddenVel", description = "Get speed per block")
    public double getVelBs() {
        final double v = oldPos.multiply(1, 0, 1).distanceTo(vPos.multiply(1, 0, 1));

        return Round(v*20, 2);
    }

    @ModuleRegisterAnnotation(Funcion = "fixVel", isHiddenFuncion = "isHiddenVel", description = "Get speed per tick with 'B/t' extension")
    public String getFixVel() {
        final double v = oldPos.multiply(1, 0, 1).distanceTo(vPos.multiply(1, 0, 1));

        return Round(v, 2) + " B/t";
    }

    @ModuleRegisterAnnotation(Funcion = "fixVelBs", isHiddenFuncion = "isHiddenVel", description = "Get speed per second with 'B/s' extension")
    public String getFixVelBs() {
        final double v = oldPos.multiply(1, 0, 1).distanceTo(vPos.multiply(1, 0, 1));

        return Round(v*20, 2)  + " B/s";
    }
//////////////////////////////////////////////////////
    public boolean isHiddenVel() {
        return !isEnable || Config.options.HiddenSpeed;
    }

    public boolean isHiddenSlime() {
        if(!MinecraftClient.getInstance().isIntegratedServerRunning()) return true;
        final long SEED = MinecraftClient.getInstance().getServer().getWorlds().iterator().next().getSeed();

        return !isEnable || !Slime.canSlimeSpawnInChunk(vPos, SEED);
    }


    public boolean isHiddenBiome() {
        return !isEnable ||  Config.options.HiddenBiome;
    }

    @Override
    public boolean isHidden() {
        return !isEnable;
    }
}
