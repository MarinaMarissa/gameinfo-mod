package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.scoreboard.ScoreboardObjective;
import net.minecraft.scoreboard.ScoreboardPlayerScore;
import net.minecraft.server.network.ServerPlayerEntity;

@ModuleRegNameAnnotation(RegName="Player")
public class mPlayer implements ModuleBase {
    ServerPlayerEntity player=null;
    ClientPlayerEntity cPlayer=null;
    public boolean isEnable = true;

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        cPlayer = tick.player;
        if(!tick.isIntegratedServerRunning())return;
        this.player = MinecraftClient.getInstance().getServer().getPlayerManager().getPlayer(cPlayer.getUuid());

    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @ModuleRegisterAnnotation(Funcion = "Score", defaultArguments = "'default'", isHiddenFuncion = "isHiddenScore", acceptArguments=true, isNeededArguments = true, description = "Get player score", Arguments = "[Objetive:String]")
    public int getScore(String Objetive){
        try {
            final ScoreboardObjective test = player.getScoreboard().getObjective(Objetive);
            final ScoreboardPlayerScore playerScore = player.getScoreboard().getPlayerScore(player.getEntityName(), test);

            return playerScore.getScore();
        }catch (Exception e){
            return  0;
        }
    }

    private EquipmentSlot getSlot(String Slot){
        final EquipmentSlot Eq;

        switch (Slot){
            case "CHEST":
                Eq=EquipmentSlot.CHEST;
                break;
            case "FEET":
                Eq=EquipmentSlot.FEET;
                break;
            case "HEAD":
                Eq=EquipmentSlot.HEAD;
                break;
            case "LEGS":
                Eq=EquipmentSlot.LEGS;
                break;
            case "OFFHAND":
                Eq=EquipmentSlot.OFFHAND;
                break;
            default:
                Eq=EquipmentSlot.MAINHAND;
                break;
        }
        return Eq;
    }

    @ModuleRegisterAnnotation(Funcion = "EquipSlotDamage", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment damage", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public int getEquipmentSlotDamage(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return 0;

        return item.getMaxDamage() - item.getDamage();
    }

    @ModuleRegisterAnnotation(Funcion = "EquipSlotMaxDamage", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment max damage", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public int getEquipmentSlotMaxDamage(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return 0;

        return item.getMaxDamage();
    }

    @ModuleRegisterAnnotation(Funcion = "EquipRepairCost", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment repair cost", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public int getEquipmentSlotRepairCost(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return 0;

        return item.getRepairCost()+1;
    }

    @ModuleRegisterAnnotation(Funcion = "EquipSlotCount", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment item count", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public int getEquipmentSlotCount(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return 0;

        return item.getCount();
    }

    @ModuleRegisterAnnotation(Funcion = "EquipSlot", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment item name", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public String getEquipmentSlot(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final Item item = cPlayer.getEquippedStack(Eq).getItem();
        if(item.equals(Items.AIR))return "None";
        return item.getName().getString();
    }

    @ModuleRegisterAnnotation(Funcion = "EquipInfo", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment reduce info", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public String getEquipmentSlotInfo(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return "";
        return  "§e" + item.getItem().getName().getString() +
                ((item.isDamageable())?":§f " + (item.getMaxDamage()-item.getDamage()) + "§e/§f" + item.getMaxDamage() : "");
    }

    @ModuleRegisterAnnotation(Funcion = "EquipInfoEx", acceptArguments=true, defaultArguments = "'MAINHAND'", description = "Get equipment extended info", Arguments = "[Slot:String(CHEST,FEET,HEAD,LEGS,OFFHAND,MAINHAND)]")
    public String getEquipmentSlotInfoEx(String Slot){
        EquipmentSlot Eq = getSlot(Slot);
        final ItemStack item = cPlayer.getEquippedStack(Eq);
        if(item.getItem().equals(Items.AIR))return "";
        return  "§e" + item.getItem().getName().getString() +
                ((item.isDamageable())?":§f " + (item.getMaxDamage()-item.getDamage()) + "§e/§f" + item.getMaxDamage() : "") +
                ((item.isDamageable())? "§e Repair: §f" + (item.getRepairCost()+1) : "")+
                ((item.getCount()>1)? "§e Count: §f" + item.getCount() : "");
    }

    @ModuleRegisterAnnotation(Funcion = "XPLevel", description = "Get player XP Level")
    public int getXPLevel(){
        return cPlayer.experienceLevel;
    }

    @ModuleRegisterAnnotation(Funcion = "XPTotal", description = "Get player total XP")
    public int getXPTotal(){
        return cPlayer.totalExperience;
    }

    @ModuleRegisterAnnotation(Funcion = "XPProgress", description = "Get player XP percentage to go up a level")
    public String getXPProgress(){
        return String.format("%.2f", cPlayer.experienceProgress);
    }

    @Override
    public boolean isHidden() {
        return !isEnable;
    }

    public boolean isHiddenScore() {
        return !isEnable || !MinecraftClient.getInstance().isIntegratedServerRunning();
    }
}
