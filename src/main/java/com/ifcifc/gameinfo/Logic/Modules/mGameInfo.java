package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import com.ifcifc.gameinfo.mixin.WorldRendererMixin;
import com.mojang.blaze3d.platform.GlDebugInfo;
import net.minecraft.SharedConstants;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.world.ServerWorld;

@ModuleRegNameAnnotation(RegName="GameInfo")
public class mGameInfo implements ModuleBase {

    int regularEntityCount, wRegularEntityCount, clientBlockEntityCount, clientTickingBlockEntityCount, serverBlockEntityCount, serverTickingBlockEntityCount;
    String ParticleCount;

    public boolean isEnable = true;

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        regularEntityCount = ((WorldRendererMixin) tick.worldRenderer).getRegularEntityCount();
        wRegularEntityCount = tick.world.getRegularEntityCount();

        clientBlockEntityCount = tick.world.blockEntities.size();
        clientTickingBlockEntityCount = tick.world.tickingBlockEntities.size();

        if (tick.isIntegratedServerRunning()) {
            final ServerWorld w = tick.getServer().getWorld(tick.world.getRegistryKey());
            serverBlockEntityCount = w.blockEntities.size();
            serverTickingBlockEntityCount = w.tickingBlockEntities.size();
        }

        ParticleCount = tick.particleManager.getDebugString();

        
    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @ModuleRegisterAnnotation(Funcion = "TotalVisibleEntityCount", description = "Quantity visible entities")
    public int getTotalVisibleEntityCount(){
        return regularEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "LoadedEntityCount", description = "Quantity world entities")
    public int getLoadedEntityCount(){
        return wRegularEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "ParticleCount", description = "Quantity of Particles")
    public String getParticleCount(){
        return ParticleCount;
    }

    @ModuleRegisterAnnotation(Funcion = "ClientBlockEntityCount", description = "Quantity client world block entities")
    public int getClientBlockEntityCount(){
        return clientBlockEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "ClientTickingBlockEntityCount", description = "Quantity client ticking block entities")
    public int getClientTickingBlockEntityCount(){
        return clientTickingBlockEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "ServerBlockEntityCount", isHiddenFuncion = "isHiddenServerBlockEntity", description = "Quantity server world block entities")
    public int getServerBlockEntityCount(){
        return serverBlockEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "ServerTickingBlockEntityCount", isHiddenFuncion = "isHiddenServerBlockEntity", description = "Quantity server ticking block entities")
    public int getServerTickingBlockEntityCount(){
        return serverTickingBlockEntityCount;
    }

    @ModuleRegisterAnnotation(Funcion = "GameVersion", description = "Game Version")
    public String getVersion(){
        return SharedConstants.getGameVersion().getName();
    }

    @ModuleRegisterAnnotation(Funcion = "ClientModName", description = "Client Mod Name")
    public String getClientModName(){
        return ClientBrandRetriever.getClientModName();
    }

    @ModuleRegisterAnnotation(Funcion = "MaxMemory", description = "Max Memory Ram")
    public long getMaxMemory(){
        return Runtime.getRuntime().maxMemory() / 1048576L;
    }
    @ModuleRegisterAnnotation(Funcion = "TotalMemory", description = "Total Memory Ram")
    public long getTotalMemory(){
        return Runtime.getRuntime().totalMemory() / 1048576L;
    }
    @ModuleRegisterAnnotation(Funcion = "FreeMemory", description = "Free Memory Ram")
    public long getFreeMemory(){
        return Runtime.getRuntime().freeMemory() / 1048576L;
    }

    @ModuleRegisterAnnotation(Funcion = "JavaVersion", description = "Java Version")
    public String getJavaVersion(){
        return System.getProperty("java.version");
    }

    @ModuleRegisterAnnotation(Funcion = "CPU", description = "Get CPU Info")
    public String getCPUInfo(){
        return GlDebugInfo.getCpuInfo();
    }
    @ModuleRegisterAnnotation(Funcion = "GPU", description = "Get GPU Info")
    public String getGPUInfo(){
        return GlDebugInfo.getRenderer();
    }

    @Override
    public boolean isHidden() {
        return !isEnable;
    }

    public boolean isHiddenServerBlockEntity() {
        return !isEnable || !MinecraftClient.getInstance().isIntegratedServerRunning();
    }
}
