package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import com.ifcifc.gameinfo.Util.WorldUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.SkyProperties;
import net.minecraft.client.world.ClientWorld;

@ModuleRegNameAnnotation(RegName="Light")
public class mLightPlayer implements ModuleBase {

    private long WorldTime  =0;
    private int  sun        =0;
    private int  light      =0;
    public boolean isEnable = true;

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        if(Config.options.HiddenLight || !(Config.options.ShowBlockLight||Config.options.ShowSunLight)){
            return;
        }

        WorldUtil W = new WorldUtil(tick.world);

        final int[] t_light = W.getLight();
        light = t_light[0];
        sun = t_light[1];

        WorldTime = W.world.getTimeOfDay();
        if(WorldTime > mTickTime.DAY) WorldTime = (long)(WorldTime% mTickTime.DAY);

    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    public String getColor(int level, boolean time){
        if(time){
            return ((sun < 8 || WorldTime > 12550 || WorldTime < 500) ? "§c" : "§f");
        }
        return ((level < 8) ? "§c" : "§f");
    }

    @Override
    public boolean isHidden() {
        return !isEnable || isHiddenBlock() && isHiddenSun();
    }

    @ModuleRegisterAnnotation(Funcion = "LightBlock", isHiddenFuncion="isHiddenBlock", description = "Get light block")
    public int getBlock(){
        return light;
    }

    @ModuleRegisterAnnotation(Funcion = "LightSun", isHiddenFuncion="isHiddenSun", description = "Get light sun")
    public int getSun(){
        return sun;
    }

    @ModuleRegisterAnnotation(Funcion = "LightFixBlock", isHiddenFuncion="isHiddenBlock", acceptArguments=true, defaultArguments = "false", description = "Get light block with color, if less than 10, Add a 0 to the front", Arguments = "[Colored:boolean]")
    public String getFixBlock(boolean colored){
        String Ret = ((light<10)? "0" : "") + light;
        return (colored)? getColor(light, false)+Ret : Ret;
    }

    @ModuleRegisterAnnotation(Funcion = "LightFixSun", isHiddenFuncion="isHiddenSun", acceptArguments=true, defaultArguments = "false", description = "Get light sun with color, if less than 10, Add a 0 to the front", Arguments = "[Colored:boolean]")
    public String getFixSun(boolean colored){
        if(Config.options.HiddenSunLightIfZero){
            if(sun==0)return "00";
        }

        String Ret = ((sun<10)? "0" : "") + sun;


        return (colored)? getColor(sun, true)+Ret : Ret;

    }

    public boolean isHiddenBlock() {
        return !isEnable || Config.options.HiddenLight || !Config.options.ShowBlockLight;
    }

    public boolean isHiddenSun() {
        ClientWorld world = MinecraftClient.getInstance().world;

        return !isEnable || Config.options.HiddenLight || !Config.options.ShowSunLight || (Config.options.HiddenSunLightIfZero && sun==0) || (!world.getSkyProperties().getSkyType().equals(SkyProperties.SkyType.NORMAL));
    }
}
