package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.MainMod;
import com.ifcifc.gameinfo.Util.TranslateText;
import com.ifcifc.gameinfo.Util.WorldUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CheckDeath {

    public CheckDeath() {
    }

    private boolean isAlive=true;

    public void checkDeath(MinecraftClient tick){

        if(tick.player.isAlive() != isAlive){
            final ClientPlayerEntity player = tick.player;
            isAlive = player.isAlive();
            WorldUtil world = new WorldUtil(tick.world);

            if(Config.options.ShowDeathMessage && !isAlive){
                long 	X = (long)player.getPos().getX(),
                        Y = (long)player.getPos().getY(),
                        Z = (long)player.getPos().getZ();

                final String DIM = world.getDimensionName();

                Config.updateDeathLog(String.format(
                        "Player: %s Time: %s - %s: %d %d %d Dim: %s",
                        player.getName().asString(),
                        new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()),
                        TranslateText.LAST_DEATH_POSITION.asString(),
                        X,Y,Z,
                        DIM
                ));

                boolean isShowDefault=true;
                try {
                    if(!tick.isIntegratedServerRunning())return;

                    ServerPlayerEntity sPlayer = tick.getServer().getPlayerManager().getPlayer(player.getUuid());
                    if(!world.dimencionEqual(sPlayer.getSpawnPointDimension().getValue()))return;
                    ///////////////////////////////////////////////////////
                    final BlockPos sp = sPlayer.getSpawnPointPosition();
                    if(null==sp)return;

                    long dist = (long) player.getPos().distanceTo(new Vec3d(
                            sp.getX(),
                            sp.getY(),
                            sp.getZ()
                    ));
                    MainMod.sendMessage(String.format(
                            "§e%s: §f%d %d %d §e%s: §f%d §eDim: §f%s",
                            TranslateText.LAST_DEATH_POSITION.asString(),
                            X,Y,Z,
                            TranslateText.DISTANCE.asString(),
                            dist,
                            DIM
                    ),false);
                    isShowDefault=false;

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    if(isShowDefault)
                        MainMod.sendMessage(String.format(
                                "§e%s: §f%d %d %d §eDim: §f%s",
                                TranslateText.LAST_DEATH_POSITION.asString(),
                                X,Y,Z,
                                DIM
                        ),false);

                }

            }
        }
    }
}
